package com.ahmadrosid.ayo_jek_driver.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ahmadrosid.ayo_jek_driver.BaseActivity;
import com.ahmadrosid.ayo_jek_driver.R;

/**
 * Created by ocittwo on 20/06/16.
 */
public class Profile extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
