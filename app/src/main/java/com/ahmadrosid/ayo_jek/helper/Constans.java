package com.ahmadrosid.ayo_jek.helper;

/**
 * Created by ocittwo on 6/18/16.
 */
public class Constans {

    public static final String DATA_USER = "data_user";
    public static final String FULL_NAME = "full_name";
    public static final String NO_HP = "no_hp";
    public static final String EMAIL = "email";
    public static final String UID = "uid";
    public static final String FCM_ID = "fcm_id";
}
